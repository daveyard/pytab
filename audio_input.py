#gravetab

#while true loop
#see if a key is pressed and apply tab modifier
#take input from audio input
#get frequency
#convert to note
#convert to tablature position
#see which position is logical
#add to tab array
#output as txt when needed

#get volume of input
#if volume greater than (whatever)
#record sample
#check pitch of sample

#used as a base
#https://stackoverflow.com/questions/40138031/how-to-read-realtime-microphone-audio-volume-in-python-and-ffmpeg-or-similar
#https://realpython.com/playing-and-recording-sound-python/#python-sounddevice_1
#https://dzone.com/articles/musical-pitch-notation

import sounddevice as sd
import os
import numpy as np
import pitch
from scipy.io.wavfile import write
import re
from math import log2, pow, log
global position
global riff_number
global positions_placed
global strings_placed
project_name = input("enter project name")
a4=440
c0=a4*pow(2,-4.75)
name = ["c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b"]
fs = 44100
seconds = 0.2
mod_string = ""
tab_contents = []
blank_tab = open('blank_tab.txt','r')
blank_tab = blank_tab.read()
blank_tab = blank_tab.split('\n')
strings_placed = []
positions_placed = []
position = 0
riff_number = 0

def reset_vars():#appears to go a bit mental when calling make noise multiple times, resetting all vars
    tab_contents = []
    blank_tab = open('blank_tab.txt','r')
    blank_tab = blank_tab.read()
    blank_tab = blank_tab.split('\n')
    strings_placed = []
    positions_placed = []
    position = 0

    strings = []
    for i in blank_tab:
        strings.append(split_all_chars(i))

    strings = np.array(strings)

def split_all_chars(word):
    return list(word)

def store_note(string_val, position):
    strings_placed.append(string_val)
    positions_placed.append(position)
    
def note_pitch(freq):
    h = round(12*log2(freq/c0))
    octave = h // 12
    n = h % 12
    note = str(name[n])+str(octave)
    #print(note)
    find_fret(freq)
    
strings = []
for i in blank_tab:
    strings.append(split_all_chars(i))

strings = np.array(strings)

class note:
    def __init__(self, string, pos, fret):
        self.string = string
        self.pos = pos
        self.fret = fret
        np.put(strings[string], pos, fret)

        
def print_sound(indata, outdata, frames, time, status):
    volume_norm = np.linalg.norm(indata)*10
    if volume_norm > 10:
        #print("playing a sound")
        sample = sd.rec(int(seconds*fs), samplerate=fs, channels=1)
        sd.wait()
        write('sample.wav',fs,sample)
        p = pitch.find_pitch('sample.wav')
        note_pitch(p)

def remove(og_string):
    global mod_string #not best practice but return just isn't working
    pattern = '[0-9]'
    mod_string = re.sub(pattern, '', og_string)

def find_fret(freq): #takes in hz
    global position
    global positions_placed
    global strings_placed
    note_freqs = ["e2","a2","d3","g3","b3","e4"]
    freq = round(freq) #provides more leniancy with no real downside
    frequencies=[82,110,146,195,246,329]
    for i in range(len(frequencies)): #find what string it's probably played on
        try:
            if frequencies[i]<=freq<frequencies[i+1]:
                print("played on ",frequencies[i])
                string_index=i
                break
            else:
                print("not played on ",frequencies[i])
        except:
            print("played on high e probably")
            string_index=i
    #find how many semitones away it is using logs
    fret_played = log(frequencies[i]/freq)/log(pow(2,1/12))
    fret_played = abs(round(fret_played))
    tab_file = open(note_freqs[i]+".txt","r")
    tablature = tab_file.read()
    tablature = str(tablature)%fret_played
    #print(tablature)
    note(abs(5-string_index), position, fret_played)
    store_note(abs(5-string_index), position)
    position += 2
    tab_contents.append(tablature)
    
    for i in strings:
        joined_str = "".join(i)
        print(joined_str)
    
        
        #calculate distance from open string to note
        #check if it's the right octave, if not add 12 frets
        #use lowest number value or closest to previously placed note
        #copy string txt replace %s

def make_noises():
    global position
    global positions_placed
    global strings_placed
    global riff_number
    with sd.Stream(callback=print_sound):
        sd.sleep(15000)
        file = open(project_name+".txt","a")
        file.write("riff %s"%riff_number+"\n")
        for i in strings:
            joined_str = "".join(i)
            file.write(joined_str+"\n")
        file.close()
        print("saved work")
        os.system('cls')
        counter = 0
        riff_number += 1
        for i in positions_placed:
            note(strings_placed[counter], i, "-")
            counter += 1
        counter = 0
        position = 0
        strings_placed = []
        positions_placed = []
        os.remove('sample.wav')
        os.system("pytab_launcher.py")

make_noises()
    


